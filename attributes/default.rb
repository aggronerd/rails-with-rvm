default.rails_app.database.username = 'user'
default.rails_app.database.password = 'password'
default.rails_app.database.host = 'host'
default.rails_app.database.name = 'name'

default.rails_app.application_user = 'appuser'
default.rails_app.user_home = '/home/appuser'
default.rails_app.code_path = '/home/appuser/application'
default.rails_app.hostname = nil
default.rails_app.repo = nil
default.rails_app.ruby_version = 'ruby-2.2.0'
default.rails_app.ruby_path = nil
default.rails_app.rubygems_version = nil
default.rails_app.code_version = 'master'
default.rails_app.secret_key_base = nil

default.rails_app.ssl.certificate_file.bucket = nil
default.rails_app.ssl.certificate_file.key = nil
default.rails_app.ssl.certificate_key_file.bucket = nil
default.rails_app.ssl.certificate_key_file.key = nil
default.rails_app.ssl.certificate_chain_file.bucket = nil
default.rails_app.ssl.certificate_chain_file.key = nil

default.passenger.module_path = nil
default.apache.listen_ports = ['80', '443']