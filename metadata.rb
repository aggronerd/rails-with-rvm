name             'rails'
maintainer       'Gregory Doran'
maintainer_email 'greg@gregorydoran.co.uk'
license          'All rights reserved'
description      'Installs/Configures news-delivery'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.2'

depends 'rvm'
depends 'apache2'

supports 'ubuntu'

recipe 'rails', 'Sets up a basic Rails app using passenger and RVM'