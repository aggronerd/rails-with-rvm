include_recipe 'rails::default'

web_app 'rails_app' do
  template 'sites/virtual_host.conf.erb'
  cookbook 'rails'
  server_name node['rails_app']['hostname']
  notifies :restart, 'service[apache2]', :delayed
end
