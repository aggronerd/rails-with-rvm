include_recipe 'rails::default'

directory '/etc/apache2/ssl' do
  owner 'apache'
  group 'apache'
  mode '0700'
  recursive true
end

package 'awscli'

keys = %w{certificate_file certificate_key_file certificate_chain_file}
keys.each do |key|

  bucket = node['rails_app']['ssl'][key]['bucket']
  bucket_key = node['rails_app']['ssl'][key]['key']

  execute "Download #{key}" do
    command "aws --region eu-west-1 s3 cp s3://#{bucket}/#{bucket_key} /etc/apache2/ssl/#{key}"
    user 'root'
  end

  file "/etc/apache2/ssl/#{key}" do
    owner 'www-data'
    mode '0600'
  end
end

apache_module 'ssl'

web_app 'rails_app_ssl' do
  template 'sites/virtual_host_ssl.conf.erb'
  cookbook 'rails'
  server_name node['rails_app']['hostname']
  notifies :restart, 'service[apache2]', :delayed
end