#
# Cookbook Name:: news-delivery
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#Create a group for the application user
group node['rails_app']['application_user'] do
  action :create
end

#Create the application user
user node['rails_app']['application_user'] do
  supports :manage_home => true
  system true
  gid node['rails_app']['application_user']
  home node['rails_app']['user_home']
  shell '/bin/bash'
  password '$6$UEY44EE1Ej$SH4jhLvaU2uv9CVU1wgmVXQBDNBqpyMh6kRpnsVqQl20UJd/6w/O0qYKpn3UFI/PEl88tKxtPG/R2/YoNM9021'
end

package 'git'

template '/tmp/mpapis.asc' do
  source 'mpapis.asc'
  cookbook 'rails'
  action :create
end

execute 'Install RVM GPG key' do
  command "HOME=\"#{node['rails_app']['user_home']}\" gpg --import /tmp/mpapis.asc"
  user node['rails_app']['application_user']
end


node.default["rvm"]["user_installs"] = [{
  'user' => node['rails_app']['application_user'],
  'default_ruby' => node['rails_app']['ruby_version'],
  'global_gems' => [{'name' => 'bundler'}],
  'rvmrc' => {'rvm_max_time_flag' => 20},
  'rubies' => [
      {'version' => node['rails_app']['ruby_version']}
  ]
}]

include_recipe 'rvm::user'

rvm_shell 'Install specific rubygems version' do
  ruby_string node['rails_app']['ruby_version']
  group node['rails_app']['application_user']
  cwd node['rails_app']['user_home']
  code "rvm rubygems #{node['rails_app']['rubygems_version']} --force"
  user node['rails_app']['application_user']
  action :run
  not_if node['rails_app']['rubygems_version'].nil?
end

rvm_gem 'bundler' do
  action :install
  ruby_string node['rails_app']['ruby_version']
  user node['rails_app']['application_user']
end

# Create the SSH key for checking out the code
directory "#{node['rails_app']['user_home']}/.ssh" do
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  recursive true
end

template "#{node['rails_app']['user_home']}/.ssh/git-deployment-key" do
  source 'ssh/git-deployment-key.erb'
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  cookbook 'rails'
  mode '0600'
  action :create
end

template "#{node['rails_app']['user_home']}/.ssh/config" do
  source 'ssh/config'
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  cookbook 'rails'
  mode '0600'
  action :create
end

template "#{node['rails_app']['user_home']}/.ssh/known_hosts" do
  source 'ssh/known_hosts'
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  cookbook 'rails'
  mode '0600'
  action :create
end

execute 'Git clone of remote repo' do
  command "git clone '#{node['rails_app']['repo']}' '#{node['rails_app']['code_path']}'"
  not_if { ::File.exists?(node['rails_app']['code_path']) }
  user node['rails_app']['application_user']
end

execute 'Git remote update' do
  command 'git remote update'
  cwd node['rails_app']['code_path']
  user node['rails_app']['application_user']
end

execute 'Git checkout of branch/tag' do
  command "git checkout '#{node['rails_app']['code_version']}'"
  cwd node['rails_app']['code_path']
  user node['rails_app']['application_user']
end

template "#{node['rails_app']['code_path']}/config/database.yml" do
  source 'rails/database.yml.erb'
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  mode '0644'
  action :create
end

rvm_shell 'Bundle install' do
  ruby_string node['rails_app']['ruby_version']
  cwd node['rails_app']['code_path']
  code 'bundle install --without development test'
  user node['rails_app']['application_user']
end

passenger_module_path = "#{node['apache']['lib_dir']}/modules/mod_passenger.so"
passenger_exists = File.exists?(passenger_module_path)

rvm_shell 'Install passenger' do
  ruby_string node['rails_app']['ruby_version']
  cwd node['rails_app']['code_path']
  code 'gem install passenger'
  user node['rails_app']['application_user']
  not_if {passenger_exists}
end

execute 'Chown chef folder' do
  command "chown -R #{node['rails_app']['application_user']} #{node['rails_app']['user_home']}/.rvm"
  cwd node['rails_app']['user_home']
  user 'root'
end

include_recipe 'apache2::default'
include_recipe 'apache2::mod_ssl'

package 'libcurl4-openssl-dev' do
  #Ignore failure - package is specific to Ubuntu/Debian
  ignore_failure true
end

package 'libcurl-devel' do
  #Ignore failure - package is specific to Amazon Linux
  ignore_failure true
end

package 'apache2-threaded-dev' do
  ignore_failure true
end

package 'httpd-devel' do
  ignore_failure true
end

package 'libapr1-dev' do
  ignore_failure true
end

package 'libaprutil1-dev' do
  ignore_failure true
end

rvm_shell 'Create mod_passenger for Apache2 module'do
  ruby_string node['rails_app']['ruby_version']
  group node['rails_app']['application_user']
  cwd node['rails_app']['user_home']
  code 'passenger-install-apache2-module -a --languages ruby'
  user node['rails_app']['application_user']
  not_if {passenger_exists}
  action :run
end

rvm_shell 'Get mod_passenger config'do
  ruby_string node['rails_app']['ruby_version']
  group node['rails_app']['application_user']
  cwd node['rails_app']['user_home']
  code 'passenger-install-apache2-module --snippet | tee /tmp/passenger-compile.log'
  user node['rails_app']['application_user']
  action :run
end

ruby_block 'Extract passenger location' do
  block do
    log_contents = File.open('/tmp/passenger-compile.log').read

    #Get passenger module location from the output
    match = log_contents.match(/LoadModule passenger_module (.+)\n/)
    raise RuntimeError, 'Could not get passenger path from the log file' if match.nil?
    node.default['passenger']['module_path'] = match[1]

    #Get the ruby path from the output
    match = log_contents.match(/PassengerDefaultRuby (.+)\n/)
    raise RuntimeError, 'Could not get ruby path from the log file' if match.nil?
    node.default['rails_app']['ruby_path'] = match[1]

    #Get the passenger path from the output
    match = log_contents.match(/PassengerRoot (.+)\n/)
    raise RuntimeError, 'Could not get passenger path from the log file' if match.nil?
    node.default['passenger']['path'] = match[1]
  end
  action :create
end

file passenger_module_path do
  owner 'root'
  group 'root'
  mode 0644
  content lazy { ::File.open(node['passenger']['module_path']).read }
  action :create
  not_if {passenger_exists}
end

apache_module 'passenger' do
  conf true
end

apache_module 'rewrite'

rvm_shell 'Database migration'do
  ruby_string node['rails_app']['ruby_version']
  group node['rails_app']['application_user']
  cwd node['rails_app']['code_path']
  code 'RAILS_ENV="production" rake db:migrate | tee /tmp/db-migration.log'
  user node['rails_app']['application_user']
  action :run
end

rvm_shell 'Generate secret' do
  ruby_string node['rails_app']['ruby_version']
  group node['rails_app']['application_user']
  cwd node['rails_app']['code_path']
  code 'rake secret > /tmp/rails_setup_secret'
  user node['rails_app']['application_user']
  action :run
end

ruby_block 'Extract secret' do
  block do
    node.default['rails_app']['secret_key_base'] = File.open('/tmp/rails_setup_secret').read.strip
  end
  action :create
end

template "#{node['rails_app']['code_path']}/config/secrets.yml" do
  source 'rails/secrets.yml.erb'
  owner node['rails_app']['application_user']
  group node['rails_app']['application_user']
  mode '0644'
  cookbook 'rails'
  action :create
end

service 'apache2' do
  action :nothing
end